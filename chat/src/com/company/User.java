package com.company;

/*
    Created by bporadzi on 2018-03-13.
 */
public class User
{
    private long id;
    private String name;
    private long numberOfStars;
    private long numberOfSentMessages;

    public User(String name, long numberOfStars, long numberOfSentMessages)
    {
        this.name = name;
        this.numberOfStars = numberOfStars;
        this.numberOfSentMessages = numberOfSentMessages;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public long getNumberOfStars()
    {
        return numberOfStars;
    }

    public void setNumberOfStars(long numberOfStars)
    {
        this.numberOfStars = numberOfStars;
    }

    public long getNumberOfSentMessages()
    {
        return numberOfSentMessages;
    }

    public void setNumberOfSentMessages(long numberOfSentMessages)
    {
        this.numberOfSentMessages = numberOfSentMessages;
    }


}
