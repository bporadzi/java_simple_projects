package com.company;

public class Main
{

    public static void main(String[] args)
    {
        // write your code here
        ChatEngine chatEngine = new ChatEngine();

        try
        {
            chatEngine.loginUser(new User("Jacek", 0, 0));
            chatEngine.loginUser(new User("Piotrek", 0, 0));
            System.out.println("Active users\t" + chatEngine.getNumberOfUsers());

            chatEngine.broadcastMessage(0, "Hehe");

            chatEngine.printStatistics();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
