package com.company;

import java.security.Key;
import java.util.*;

import static jdk.nashorn.internal.objects.NativeMath.max;

/*
    Created by bporadzi on 2018-03-13.
 */
class ChatEngine
{
    private Map<Long, User> users;
    private Map<String, Long> userNames;

    private long id;

    ChatEngine()
    {
        users = new HashMap<>();
        userNames = new HashMap<>();
        id = 0;
    }

    void loginUser(User user) throws UserLoginException
    {
        if (users.containsValue(user))
        {
            throw new UserLoginException();
        }

        user.setId(id);
        userNames.put(user.getName(), id);
        users.put(id++, user);
    }

    boolean broadcastMessage(long userId, String message)
    {
        if (!users.containsKey(userId))
        {
            return false;
        }
        users.get(userId).setNumberOfSentMessages(users.get(userId).getNumberOfSentMessages() + 1);
        for (HashMap.Entry<Long, User> user : users.entrySet())
        {
            System.out.println("Message sent to user " + user.getKey() + " " + user.getValue().getName());
        }
        return true;
    }

    void logoutUser(long userId) throws UserRemoveException
    {
        if (!users.containsKey(userId))
        {
            throw new UserRemoveException();
        }

        userNames.remove(users.get(userId).getName());
        users.remove(userId);
    }

    void printStatistics()
    {
        int sum = 0;
        for (HashMap.Entry<Long, User> user : users.entrySet())
        {
            sum += user.getValue().getNumberOfSentMessages();
        }
        System.out.println("Average " + 1.0 * sum / users.size());
    }

    int getNumberOfUsers()
    {
        return users.size();
    }


    void addUserStar(String userName)
    {
        if (userNames.containsKey(userName))
        {
            Long userId = userNames.get(userName);
            users.get(userId).setNumberOfStars(users.get(userId).getNumberOfStars() + 1);
        }
    }


    void removeUserStar(String userName)
    {
        if (userNames.containsKey(userName))
        {
            Long userId = userNames.get(userName);
            users.get(userId).setNumberOfStars(users.get(userId).getNumberOfStars() - 1);
        }
    }

    boolean hasUser(long userId)
    {
        return users.containsKey(userId);
    }

    boolean hasUser(String userName)
    {
        return userNames.containsKey(userName);
    }
}
