package sample;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.InputEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Calendar;
import java.util.EventObject;
import java.util.HashMap;

import static java.lang.Thread.sleep;

public class Controller {
    @FXML
    private Canvas canvasHours;

    @FXML
    private Canvas canvasSeparator1;

    @FXML
    private Canvas canvasMinutes;

    @FXML
    private Canvas canvasSeparator2;

    @FXML
    private Canvas canvasSeconds;

    @FXML
    private MenuItem buttonNewAlarm;

    @FXML
    private TableView tableAlarms;

    @FXML
    private TableColumn columnTime;

    @FXML
    private TableColumn columnMessage;

    @FXML
    private MenuItem buttonDeleteAll;

    @FXML
    private MenuItem buttonClose;

    @FXML
    private MenuItem aboutMe;

    private ObservableList<Alarm> data;

    public Controller() {
        Platform.runLater(this::runClock);
        Platform.runLater(this::initializeAlarms);
    }

    private void initializeAlarms() {
        tableAlarms.setEditable(true);
        ContextMenu contextMenu = new ContextMenu();

        MenuItem item1 = new MenuItem("Delete row");

        item1.setOnAction(event ->
        {
            Object selectedItem = tableAlarms.getSelectionModel().getSelectedItem();
            tableAlarms.getItems().remove(selectedItem);
        });

        contextMenu.getItems().add(item1);

        tableAlarms.addEventHandler(MouseEvent.MOUSE_CLICKED, t -> {
            if (t.getButton() == MouseButton.SECONDARY) {
                contextMenu.show(tableAlarms, t.getScreenX(), t.getScreenY());
            }
            if(t.getButton() == MouseButton.PRIMARY && contextMenu.isFocused())
            {
                contextMenu.hide();
            }
        });

        data = FXCollections.observableArrayList();
    }

    @FXML
    private void initialize()
    {
        buttonClose.setOnAction(t ->
        {
            Platform.exit();
            System.exit(0);
        });
        buttonDeleteAll.setOnAction(t->
        {
            data.clear();
            showTableData();
        });
        aboutMe.setOnAction(t->
        {
            try
            {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("about.fxml"));
                Scene scene = new Scene(fxmlLoader.load());
                Stage stage = new Stage();
                stage.setTitle("Author info");
                stage.setScene(scene);
                stage.show();
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        });
    }

    private void runClock() {
        buttonNewAlarm.setOnAction(t -> {
            try {
                AlarmMessageController wc = new AlarmMessageController();
                wc.showStage();

                data.add(new Alarm(wc.getSelectedHour(), wc.getSelectedMinute(), wc.getAlarmMessage()));

                showTableData();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        new Thread(this::animateSeparators).start();

        new Thread(this::refreshTime).start();
    }

    private void showTableData() {
        TableColumn hoursCol = columnTime;
        hoursCol.setCellValueFactory(new PropertyValueFactory<Alarm, String>("hour"));

        TableColumn messagesCol = columnMessage;
        messagesCol.setCellValueFactory(
                new PropertyValueFactory<Alarm, String>("message")
        );

        tableAlarms.setItems(data);
    }


    private void refreshTime() {
        while (true) {
            try {
                sleep(1000);
                clearTime();
            } catch (Exception e) {
            } finally {
                Calendar now = Calendar.getInstance();

                int hour = now.get(Calendar.HOUR_OF_DAY);
                int minute = now.get(Calendar.MINUTE);

                new Thread(() -> checkAlarms(hour, minute)).start();

                GraphicsContext gc = canvasHours.getGraphicsContext2D();
                gc.setTextAlign(TextAlignment.CENTER);
                gc.setTextBaseline(VPos.CENTER);
                gc.setFont(new Font("Arial", 70));
                gc.strokeText(hour + "", canvasHours.getWidth() / 2, canvasHours.getHeight() / 2);

                gc = canvasMinutes.getGraphicsContext2D();
                gc.setTextAlign(TextAlignment.CENTER);
                gc.setTextBaseline(VPos.CENTER);
                gc.setFont(new Font("Arial", 70));
                gc.strokeText(minute + "", canvasMinutes.getWidth() / 2, canvasMinutes.getHeight() / 2);

                gc = canvasSeconds.getGraphicsContext2D();
                gc.setTextAlign(TextAlignment.CENTER);
                gc.setTextBaseline(VPos.CENTER);
                gc.setFont(new Font("Arial", 30));
                gc.strokeText(now.get(Calendar.SECOND) + "", canvasSeconds.getWidth() / 2, canvasSeconds.getHeight() / 2);
            }
        }
    }

    private void checkAlarms(int hour, int minute) {
        for (Alarm alarm : data) {
            if (Integer.parseInt(alarm.getOnlyHour()) == hour && Integer.parseInt(alarm.getOnlyMinute()) == minute) {
                ringFor(alarm);
            }
        }
    }

    private void ringFor(Alarm alarm) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Alarm");
            alert.setHeaderText("Alarm message");
            String s = alarm.getMessage();
            alert.setContentText(s);
            alert.show();
            data.remove(alarm);
            showTableData();
        });

    }

    private void clearTime() {
        GraphicsContext gc = canvasHours.getGraphicsContext2D();
        gc.clearRect(0, 0, canvasHours.getWidth(), canvasHours.getHeight());
        gc = canvasMinutes.getGraphicsContext2D();
        gc.clearRect(0, 0, canvasMinutes.getWidth(), canvasMinutes.getHeight());
        gc = canvasSeconds.getGraphicsContext2D();
        gc.clearRect(0, 0, canvasSeconds.getWidth(), canvasSeconds.getHeight());
    }

    private void animateSeparators() {
        while (true) {
            try {
                sleep(500);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                GraphicsContext gc = canvasSeparator1.getGraphicsContext2D();
                gc.setTextAlign(TextAlignment.CENTER);
                gc.setTextBaseline(VPos.CENTER);
                gc.strokeText(":", canvasSeparator1.getWidth() / 2, canvasSeparator1.getHeight() / 2);

                gc = canvasSeparator2.getGraphicsContext2D();
                gc.setTextAlign(TextAlignment.CENTER);
                gc.setTextBaseline(VPos.CENTER);
                gc.strokeText(":", canvasSeparator2.getWidth() / 2, canvasSeparator2.getHeight() / 2);


                try {
                    sleep(200);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                clearSeparators();
            }

        }
    }

    private void clearSeparators() {
        GraphicsContext gc = canvasSeparator1.getGraphicsContext2D();
        gc.clearRect(0, 0, canvasSeparator1.getWidth(), canvasSeparator1.getHeight());
        gc = canvasSeparator2.getGraphicsContext2D();
        gc.clearRect(0, 0, canvasSeparator2.getWidth(), canvasSeparator2.getHeight());
    }

}