package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;

public class About {
    @FXML
    private Label name;

    @FXML
    private Label surname;

    @FXML
    private Label mail;

    @FXML
    private ImageView imageView;

    @FXML
    private void initialize()
    {
        name.setText("Bartlomiej");
        surname.setText("Poradzisz");
        mail.setText("bartekporadzisz@gmail.com");

        imageView.setImage(new Image("avatar.png"));

    }
}
