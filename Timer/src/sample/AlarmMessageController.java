package sample;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

public class AlarmMessageController extends Stage {

    @FXML
    private ComboBox comboHour;

    @FXML
    private ComboBox comboMinute;

    @FXML
    private Button buttonAdd;

    @FXML
    private TextArea message;

    private Stage stage;
    private static String selectedHour;
    private static String selectedMinute;
    private static String alarmMessage;

    private static String[] HOURS = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
            "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"};

    private static String[] MINUTES = {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
            "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47",
            "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59"};

    public AlarmMessageController() {

    }

    @FXML
    private void addAlarm() {
        {

        }
    }


    void showStage() {
        stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);

        try {
            GridPane root = FXMLLoader.load(getClass().getResource("AlarmMessage.fxml"));
            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.showAndWait();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String getSelectedHour() {
        return selectedHour;
    }

    String getSelectedMinute() {
        return selectedMinute;
    }

    String getAlarmMessage()
    {
        return alarmMessage;
    }

    @FXML
    public void initialize() {
        buttonAdd.setOnAction(e -> {
            if (comboHour.getValue() != null && comboMinute != null) {
                selectedHour = comboHour.getValue().toString();
                selectedMinute = comboMinute.getValue().toString();
                alarmMessage = message.getText();
                try {
                    ((Stage) buttonAdd.getScene().getWindow()).close();
                    //                    stage.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        comboHour.getItems().removeAll(comboHour.getItems());
        comboHour.getItems().addAll(HOURS);

        comboMinute.getItems().removeAll(comboMinute.getItems());
        comboMinute.getItems().addAll(MINUTES);
    }
}
