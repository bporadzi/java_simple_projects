package sample;

        import javafx.beans.property.SimpleStringProperty;

public class Alarm {

    private final SimpleStringProperty hour;
    private final SimpleStringProperty message;

    Alarm(String hour, String minute, String message) {
        this.hour = new SimpleStringProperty(hour + ":" + minute);
        this.message = new SimpleStringProperty(message);
    }

    public String getOnlyHour()
    {
        String h = hour.get();

        return h.substring(0, h.indexOf(":") );
    }

    public String getOnlyMinute()
    {
        String h = hour.get();

        return h.substring(h.indexOf(":")+1);
    }

    public String getHour() {
        return hour.get();
    }

    public String getMessage() {
        return message.get();
    }
}
