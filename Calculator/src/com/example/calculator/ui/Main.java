package com.example.calculator.ui;

import com.example.calculator.logic.ConeCalculator;
import com.example.calculator.logic.ExtendedConeCalculator;
import com.example.calculator.logic.ExtendedTetrahedronCalculator;
import com.example.calculator.logic.TetrahedronCalculator;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/*
  Created by bporadzi on 2018-03-06.
 */
public class Main
{
    public static void main(String[] args)
    {
        char choice;
        while (true)
        {
            try
            {
                System.out.println("1. Calculate cone base");
                System.out.println("2. Calculate cone base parimeter");
                System.out.println("3. Calculate  Tetrahedron base");
                System.out.println("4. Calculate Tetrahedron parimeter");
                System.out.println("5. Calculate cone area");
                System.out.println("6. Calculate cone volume");
                System.out.println("7. Calculate Tetrahedron area");
                System.out.println("8. Calculate Tetrahedron volume");
                System.out.println("q. Exit");
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
                choice = bufferedReader.readLine().charAt(0);
                if (choice == 'q')
                {
                    return;
                }
                switch (choice)
                {
                    case '1':
                    {
                        System.out.println(new ConeCalculator().calculateBaseArea());
                        break;
                    }
                    case '2':
                    {
                        System.out.println(new ConeCalculator().calculateBasePerimeter());
                        break;
                    }
                    case '3':
                    {
                        System.out.println(new TetrahedronCalculator().calculateBaseArea());
                        break;
                    }
                    case '4':
                    {
                        System.out.println(new TetrahedronCalculator().calculateBasePerimeter());
                        break;
                    }
                    case '5':
                    {
                        new ExtendedConeCalculator().calculateArea();
                        break;
                    }
                    case '6':
                    {
                        new ExtendedConeCalculator().calculateVolume();
                        break;
                    }
                    case '7':
                    {
                        new ExtendedTetrahedronCalculator().calculateArea();
                        break;
                    }
                    case '8':
                    {
                        new ExtendedTetrahedronCalculator().calculateVolume();
                        break;
                    }
                }
            } catch (Exception e)
            {

            }
        }

    }
}
