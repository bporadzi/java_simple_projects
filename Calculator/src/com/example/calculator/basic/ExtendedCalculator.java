package com.example.calculator.basic;

/*
    Created by bporadzi on 2018-03-06.
 */
public interface ExtendedCalculator extends Calculator
{
    void calculateArea() throws Exception;

    void calculateVolume() throws Exception;
}
