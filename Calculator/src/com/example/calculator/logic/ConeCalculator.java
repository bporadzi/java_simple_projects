package com.example.calculator.logic;

import com.example.calculator.basic.Calculator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
    Created by bporadzi on 2018-03-06.
 */
public class ConeCalculator implements Calculator {
    @Override
    public double calculateBaseArea() {
        try {
            double r;
            System.out.println("Enter range");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            r = Double.parseDouble(bufferedReader.readLine());
            return Math.PI * Math.pow(r, 2);
        } catch (IOException e) {
            return -1;
        }
    }

    @Override
    public double calculateBasePerimeter() {
        try
        {
            double r;
            System.out.println("Enter range");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            r = Double.parseDouble(bufferedReader.readLine());

            return Math.PI * 2 * r;
        }catch (IOException e)
        {
            return -1;
        }
    }
}
