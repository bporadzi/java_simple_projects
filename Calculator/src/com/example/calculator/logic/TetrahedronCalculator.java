package com.example.calculator.logic;

import com.example.calculator.basic.Calculator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
  Created by bporadzi on 2018-03-06.
 */
public class TetrahedronCalculator implements Calculator
{
    @Override
    public double calculateBaseArea()
    {
        try
        {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter base edge");
            double edgeLength = Double.parseDouble(bufferedReader.readLine());

            return Math.pow(edgeLength, 2) * Math.sqrt(3) / 4;
        } catch (IOException e)
        {
            return -1;
        }
    }

    @Override
    public double calculateBasePerimeter()
    {
        try
        {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter first base edge");
            double edgeLength = Double.parseDouble(bufferedReader.readLine());

            return edgeLength * 3;
        } catch (IOException e)
        {
            return -1;
        }
    }
}
