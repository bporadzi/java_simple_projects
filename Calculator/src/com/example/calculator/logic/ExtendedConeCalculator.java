package com.example.calculator.logic;

import com.example.calculator.basic.ExtendedCalculator;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/*
    Created by bporadzi on 2018-03-06.
 */
public class ExtendedConeCalculator extends ConeCalculator implements ExtendedCalculator
{
    @Override public void calculateArea() throws Exception
    {
        double r = super.calculateBaseArea()/Math.PI;
    }

    @Override public void calculateVolume() throws Exception
    {
        System.out.println("Enter cone height");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        double height = Double.parseDouble(bufferedReader.readLine());
        System.out.println("Volume is " + (height * new ConeCalculator().calculateBaseArea()) / 3);
    }
}
