package com.example.calculator.logic;

import com.example.calculator.basic.ExtendedCalculator;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/*
    Created by bporadzi on 2018-03-06.
 */
public class ExtendedTetrahedronCalculator extends TetrahedronCalculator implements ExtendedCalculator
{
    @Override public void calculateArea() throws Exception
    {
        System.out.println("Total area is " + super.calculateBaseArea() * 4);
    }

    @Override public void calculateVolume() throws Exception
    {
        System.out.println("Enter height");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        double height = Double.parseDouble(bufferedReader.readLine());
        System.out.println("Total area is " + super.calculateBaseArea() * height / 3);
    }
}
