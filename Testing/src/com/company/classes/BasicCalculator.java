package com.company.classes;

public class BasicCalculator {
    public double calculateSum(double d1, double d2) {
        return d1 + d2;
    }

    public double calculateDifference(double d1, double d2) {
        return d1 - d2;
    }

    public double calculateMultiplication(double d1, double d2) {
        return d1 * d2;
    }

    public double calculateDivision(double d1, double d2) {
        if (d2 == 0) throw new IllegalArgumentException();
        return d1 / d2;
    }

    public double calculatePow(double d1, double d2) {
        return Math.pow(d1, d2);
    }

    public double calculateSqrt(double d1) {
        if (d1 < 0) throw new IllegalArgumentException();
        return Math.sqrt(d1);
    }

}


