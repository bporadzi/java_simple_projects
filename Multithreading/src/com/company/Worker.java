package com.company;

import javafx.util.Pair;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class Worker implements Runnable {

    private String workerName;

    private boolean isThreadStarted;
    private boolean isThreadWorking;

    private boolean locked = false;

    private WorkerListener workerListener;

    private final LinkedHashMap<Pair<String, Integer>, Task> taskHashMap;

    private static int counter = 0;

    private Thread mainThread;

    Worker(String workerName) {
        this.workerName = "Worker " + workerName + " Thread";
        taskHashMap = new LinkedHashMap<>();
        mainThread = new Thread(this);
    }

    void enqueueTask(String taskName, Task task) {
        synchronized (taskHashMap) {
            Pair<String, Integer> pair = new Pair<>(taskName, counter++);
            taskHashMap.put(pair, task);
        }
    }

    void interrupt() {
        isThreadStarted = false;
        mainThread.interrupt();
    }

    void start() {
        if (isStarted()) return;

        while (locked) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        workerListener.onWorkerStarted();

        isThreadStarted = true;
        mainThread.start();
    }

    void stop() {
        if (!isStarted()) return;

        isThreadStarted = false;
        locked = true;

        try {
            while (isWorking()) {
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            locked = false;
            workerListener.onWorkerStopped();
        }
    }

    void setListener(WorkerListener workerListener) {
        this.workerListener = workerListener;
    }

    private boolean isStarted() {
        return isThreadStarted;
    }

    private boolean isWorking() {
        return isThreadWorking;
    }

    @Override
    public void run() {
        System.out.println("Current " + workerName);

        while (isThreadStarted) {

            if (taskHashMap.size() == 0) {
                isThreadWorking = false;
                System.out.println("Waiting for tasks");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    continue;
                }
            }

            isThreadWorking = true;

            HashMap.Entry entry;

            synchronized (taskHashMap) {
                entry = taskHashMap.entrySet().iterator().next();
            }

            if (entry == null) {
                continue;
            }

            Pair pair = (Pair) entry.getKey();

            int id = (int) pair.getValue();

            workerListener.onTaskStarted(id, (String) pair.getKey());

            System.out.println("Executing task '" + pair.getKey() + "' from " + workerName);

            try {
                ((Task) entry.getValue()).run(id);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                synchronized (taskHashMap) {
                    taskHashMap.remove(pair);
                }
                workerListener.onTaskCompleted(id, (String) pair.getKey());
            }
        }
        isThreadWorking = false;
    }

}
