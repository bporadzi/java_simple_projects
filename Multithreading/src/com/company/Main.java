package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.lang.Thread.*;
import static java.lang.Thread.currentThread;
import static java.lang.Thread.sleep;

public class Main {

    public static void main(String[] args) {
        List<Worker> workerList = new ArrayList<>();

        for (int i = 0; i < 1; i++) {
            Worker worker = new Worker("w. " + i);

            worker.setListener(new WorkerListener() {
                @Override
                public void onWorkerStarted() {
                    System.out.println("worker started");
                }

                @Override
                public void onWorkerStopped() {
                    System.out.println("worker stopped");
                }

                @Override
                public void onTaskStarted(int taskNumber, String taskName) {
                    System.out.println("task " + taskNumber + "'" + taskName + "'" + " started");
                }

                @Override
                public void onTaskCompleted(int taskNumber, String taskName) {
                    System.out.println("task " + taskNumber + "'" + taskName + "'" + " completed");
                }
            });

            worker.start();
            addTasks(worker);
            workerList.add(worker);
        }

        new Thread(() ->
        {
            try {
                sleep(15000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                for (Worker worker : workerList) {
                    worker.stop();
                }

                for (Worker worker : workerList) {
                    worker.interrupt();
                }
            }
        }).start();
    }

    private static void setTimeout(Runnable runnable) {
        new Thread(() -> {
            try {
                sleep(10000);
                runnable.run();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    private static void addTasks(Worker worker) {

        worker.enqueueTask("100%", taskNumber -> {
            AtomicBoolean working = new AtomicBoolean(true);
            setTimeout(() -> working.set(false));

            Random rng = new Random();

            double store = 1;

            while (working.get()) {

                if (interrupted())
                    throw new InterruptedException("InterruptedException " + currentThread().getName());

                double r = rng.nextFloat();
                double v = Math.sin(Math.cos(Math.sin(Math.cos(r))));
                store *= v;
            }

        });


        worker.enqueueTask("0%", taskNumber -> {
            try {
                sleep(10000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        });


        worker.enqueueTask("yield", taskNumber -> {
            AtomicBoolean working = new AtomicBoolean(true);
            setTimeout(() -> working.set(false));

            while (working.get()) {
                if (interrupted())
                    throw new InterruptedException("InterruptedException " + currentThread().getName());

                yield();
            }
        });


        worker.enqueueTask("empty", taskNumber -> {
            try {
                sleep(10000);
            } catch (InterruptedException ex) {
                throw new InterruptedException("InterruptedException " + currentThread().getName());
            }
        });


        worker.enqueueTask("empty", taskNumber -> {
            try {
                sleep(10000);
            } catch (InterruptedException ex) {
                throw new InterruptedException("InterruptedException " + currentThread().getName());
            }
        });
    }
}
