package swing;

import javax.swing.JOptionPane;

public class LoginFrame extends javax.swing.JFrame {

    public LoginFrame() {
        initComponents();
    }

    private boolean validateUsername()
    {
        int usernameLength = textboxStudent.getText().length();
        
        boolean isGood = usernameLength >= 3 && usernameLength <= 20;
        
        labelWrongUser.setVisible(!isGood);
        
        return isGood;
    }

    private boolean validatePassword()
    {
        int passwordLength = passwordField.getPassword().length;
        boolean isGood = passwordLength >= 3 && passwordLength <= 20;
        
        labelWrongPassword.setVisible(!isGood);
        
        return isGood;
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelStudent = new javax.swing.JLabel();
        textboxStudent = new javax.swing.JTextField();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0));
        labelWrongUser = new javax.swing.JLabel();
        labelPassword = new javax.swing.JLabel();
        passwordField = new javax.swing.JPasswordField();
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        labelWrongPassword = new javax.swing.JLabel();
        filler3 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        buttonLogin = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridLayout(5, 2));

        labelStudent.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelStudent.setText("Student name");
        getContentPane().add(labelStudent);

        textboxStudent.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        textboxStudent.setText("name");
        textboxStudent.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                textboxStudentFocusGained(evt);
            }
        });
        getContentPane().add(textboxStudent);
        getContentPane().add(filler1);

        labelWrongUser.setForeground(new java.awt.Color(255, 0, 51));
        labelWrongUser.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelWrongUser.setVisible(false);
        labelWrongUser.setText("Wrong username");
        getContentPane().add(labelWrongUser);

        labelPassword.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelPassword.setText("Student password");
        getContentPane().add(labelPassword);

        passwordField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        passwordField.setText("password");
        getContentPane().add(passwordField);
        getContentPane().add(filler2);

        labelWrongPassword.setForeground(new java.awt.Color(255, 51, 51));
        labelWrongPassword.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelWrongPassword.setVisible(false);
        labelWrongPassword.setText("Wrong password");
        getContentPane().add(labelWrongPassword);
        getContentPane().add(filler3);

        buttonLogin.setText("Login");
        buttonLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonLoginActionPerformed(evt);
            }
        });
        getContentPane().add(buttonLogin);

        jMenu1.setText("Program");

        jMenuItem1.setText("Close");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void textboxStudentFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_textboxStudentFocusGained
        
    }//GEN-LAST:event_textboxStudentFocusGained

    private void buttonLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonLoginActionPerformed
        if(validateUsername() && validatePassword())
        {
            new UsersFrame(textboxStudent.getText(), passwordField.getPassword().toString()).setVisible(true);
            this.setVisible(false);
        }else
        {
            JOptionPane.showConfirmDialog(null, "Wrong login or password", "Error occured", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_buttonLoginActionPerformed

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonLogin;
    private javax.swing.Box.Filler filler1;
    private javax.swing.Box.Filler filler2;
    private javax.swing.Box.Filler filler3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JLabel labelPassword;
    private javax.swing.JLabel labelStudent;
    private javax.swing.JLabel labelWrongPassword;
    private javax.swing.JLabel labelWrongUser;
    private javax.swing.JPasswordField passwordField;
    private javax.swing.JTextField textboxStudent;
    // End of variables declaration//GEN-END:variables
}
