package system;

public class ScatterSystem {

    public ScatterSystem() {

    }

    public double makeOperation(double[] args) {

        double result = 1;

        for (int i = 0; i < 100; i++) {
            for (double arg : args)
                result *= arg * Math.cos(i);
        }

        return result;
    }
}
