package system;

import java.io.*;
import java.util.*;

public class ExtendedSystemCache extends SystemCache implements Serializable {

    private static String endOfLine = System.getProperty("line.separator");

    public void importCSV(String path) throws IOException {
        importCSV(new File(path));
    }

    private void importCSV(File file) throws IOException {
        try (InputStream inputStream = new FileInputStream(file)) {
            importCSV(inputStream);
        }
    }

    private void importCSV(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        String line;

        while ((line = bufferedReader.readLine()) != null) {
            int beginIndex = 0;
            int endIndex = line.indexOf(',', beginIndex);

            List<Double> key = new ArrayList<>();
            Double value = Double.parseDouble(line.substring(beginIndex, endIndex));
            beginIndex = endIndex + 1;
            while (beginIndex > 0) {
                endIndex = line.indexOf(',', beginIndex);
                if (endIndex == -1) {
                    key.add(Double.parseDouble(line.substring(beginIndex)));
                } else {
                    key.add(Double.parseDouble(line.substring(beginIndex, endIndex)));
                }
                beginIndex = endIndex + 1;
            }

            cache.put(new Parameter(key.stream().mapToDouble((d -> d)).toArray()), value);
        }
    }

    public void exportCSV(String path) throws IOException {
        exportCSV(new File(path));
    }

    private void exportCSV(File file) throws IOException {
        try (OutputStream outputStream = new FileOutputStream(file)) {
            exportCSV(outputStream);
        }
    }

    private void exportCSV(OutputStream outputStream) throws IOException {
        outputStream.write(getCSVData().getBytes());
    }

    public void serialize(String path) throws IOException {
        serialize(new File(path));
    }

    private void serialize(File file) throws IOException {
        try (OutputStream outputStream = new FileOutputStream(file)) {
            serialize(outputStream);
        }
    }

    private void serialize(OutputStream outputStream) throws IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(cache);
    }

    public void deserialize(String path) throws IOException {
        deserialize(new File(path));
    }

    private void deserialize(File file) throws IOException {
        try (InputStream inputStream = new FileInputStream(file)) {
            deserialize(inputStream);
        }
    }

    @SuppressWarnings("all")
    private void deserialize(InputStream inputStream) throws IOException {
        cache = null;
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);

        try {
            cache = (HashMap<Parameter, Double>) objectInputStream.readObject();
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found");
            e.printStackTrace();
        }
    }

    public void save(String path) throws IOException {
        save(new File(path));
    }

    private void save(File file) throws IOException {
        try (OutputStream outputStream = new FileOutputStream(file)) {
            save(outputStream);
        }
    }

    private void save(OutputStream outputStream) throws IOException {
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);

        for (HashMap.Entry entry : cache.entrySet()) {
            Parameter parameter = (Parameter) entry.getKey();

            double[] key = parameter.getValues();

            dataOutputStream.writeInt(key.length);

            dataOutputStream.writeDouble((Double) entry.getValue());

            for (double d : key) {
                dataOutputStream.writeDouble(d);
            }
        }
    }

    public void load(String path) throws IOException {
        load(new File(path));
    }

    private void load(File file) throws IOException {
        try (InputStream inputStream = new FileInputStream(file)) {
            load(inputStream);
        }
    }

    private void load(InputStream inputStream) throws IOException {
        DataInputStream dataIn = new DataInputStream(inputStream);

        while (true) {
            try {
                int numOfDoubles = dataIn.readInt();

                List<Double> key = new ArrayList<>();


                double value = dataIn.readDouble();

                for (int i = 0; i < numOfDoubles; i++) {
                    key.add(dataIn.readDouble());
                }

                cache.put(new Parameter(key.stream().mapToDouble((d -> d)).toArray()), value);
            } catch (EOFException e) {
                break;
            }
        }
    }

    private String getCSVData() {
        StringBuilder output = new StringBuilder();

        for (HashMap.Entry entry : cache.entrySet()) {

            Parameter p = (Parameter) entry.getKey();

            output.append(entry.getValue());

            for (double d : p.getValues()) {
                output.append(",");
                output.append(d);
            }

            output.append(endOfLine);
        }
        return output.toString();
    }

}
