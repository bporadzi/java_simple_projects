package system;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;

public class SystemCache {

    transient HashMap<Parameter, Double> cache = new HashMap<>();

    public void showResults() {
        for (HashMap.Entry entry : cache.entrySet()) {

            Parameter parameter = (Parameter) entry.getKey();

            for (double d : parameter.getValues()) {
                System.out.print(d + " ");
            }

            System.out.println("\t" + entry.getValue());
        }
    }


    public void put(double[] input, double output) {
        this.cache.put(new Parameter(input), output);
    }

    public Double get(double[] input) {
        return this.cache.get(new Parameter(input));
    }


}
