package system;

import java.io.Serializable;
import java.util.Arrays;

public class Parameter implements Serializable
{
    private double[] values;

    public Parameter(double[] values) {
        this.values = values;
    }

    @Override
    public int hashCode() {
        return 31 + Arrays.hashCode(this.values);
    }

    public double[] getValues() {
        return values;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (this.getClass() != obj.getClass())
            return false;
        Parameter other = (Parameter) obj;
        return Arrays.equals(this.values, other.values);
    }
}