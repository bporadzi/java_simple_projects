package utilities;

import system.Parameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Util
{
    public static void parseLine(HashMap<Parameter, Double> hashMap, String line)
    {
        List<Double> args = new ArrayList<>();

        int beginIndex = 0;
        int endIndex;
        do
        {
            endIndex = line.indexOf(" ", beginIndex);
            if (endIndex > 0)
            {
                Double num = Double.parseDouble(line.substring(beginIndex, endIndex));
                beginIndex = endIndex + 1;
                args.add(num);
            } else
            {
                int separatorIndex = line.indexOf(",");
                args.add(Double.parseDouble(line.substring(beginIndex, separatorIndex)));

                Double val = Double.parseDouble(line.substring(separatorIndex + 1));

                double[] keys = args.stream().mapToDouble((d -> d)).toArray();

                hashMap.put(new Parameter(keys), val);
                break;
            }
        } while (endIndex <= line.length());
    }

}
