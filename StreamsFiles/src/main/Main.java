package main;

import system.ExtendedSystemCache;
import system.ScatterSystem;

import java.io.IOException;

public class Main
{
    public static void main(String[] args)
    {
        ScatterSystem system = new ScatterSystem();
        ExtendedSystemCache cache = new ExtendedSystemCache();

        try {
            cache.importCSV("exported.csv");
        } catch (IOException e) {
            e.printStackTrace();
        }

        cache.showResults();

//        for (int i = 0; i < 3; i++)
//        {
//            double[] input = {};
//            switch (i)
//            {
//                case 0:
//                {
//                    input = new double[]{1, 2, 3, 6, 8};
//                    break;
//                }
//                case 1:
//                {
//                    input = new double[]{2, 3, 3, 6, 8};
//                    break;
//                }
//                case 2:
//                {
//                    input = new double[]{1, 3, 3, 6, 8};
//                    break;
//                }
//            }
//            Double output = cache.get(input);
//            if (output == null)
//            {
//                output = system.makeOperation(input);
//                cache.put(input, output);
//            }
//        }
//        try
//        {
//            cache.exportCSV("exported.csv");
//        }catch (IOException e)
//        {
//            e.printStackTrace();
//        }

        try {
            cache.save("save.txt");
            cache.load("save.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("After loading:");
        cache.showResults();

//        try {
//            cache.exportCSV("test.csv");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        cache.showResults();

    }
}
